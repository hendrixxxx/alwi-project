<?php
include '../../koneksi/koneksi.php';		  //agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include
?>
<html>
<style>
	table.center {
		margin-left: auto;
		margin-right: auto;
	}
</style>

<body>
	<h3 style="text-align: center;">DATA JADWAL</h3>
	<a href="../view/konsumen/input_konsumen.php"></a>
	<table border="1" class="table center">
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Jemput</th>
			<th>Jam</th>
			<th>Tanggal</th>
			<th>Tujuan</th>
			<th>Armada</th>
			<th>Crew</th>
			<th>Biaya</th>
			<th>Action</th>

		</tr>
		<?php
		$no = 1;

		$data = mysqli_query(
			$koneksi,
			"SELECT 
		 jadwal.id as id_jadwal,
		 jadwal.*,
		 crew.nama as crew_name,
		 armada.*,
		 tujuan.nama as tujuan_name
	
		  from 
		  jadwal
		 LEFT JOIN crew 	ON jadwal.crew = crew.id
		 LEFT JOIN armada 	ON jadwal.armada = armada.id
		 LEFT JOIN tujuan 	ON jadwal.tujuan = tujuan.id
		 "
		);
		if ($data) {
			# code...

			while ($d = mysqli_fetch_array($data)) {
				// print_r($d);exit;
		?>

				<tr>
					<td><?php echo  $no++; ?></td>
					<td><?php echo $d['nama']; ?></td>
					<td><?php echo $d['alamat']; ?></td>
					<td><?php echo $d['jemput']; ?></td>
					<td><?php echo $d['jam']; ?></td>
					<td><?php echo $d['tanggal']; ?></td>
					<td><?php echo $d['tujuan_name']; ?></td>
					<td><?php echo $d['jenis_armada'] . ' || Seat: ' . $d['jumlah_seat']; ?></td>
					<td><?php echo $d['crew_name']; ?></td>
					<td><?php echo $d['biaya']; ?></td>
					<td><a href="../../controller/hapus/delet_jadwal.php?id=<?php echo $d['id_jadwal']; ?>">Hapus</a> || <a href="hapus.php?id=<?php echo $d['id']; ?>">edit</a></td>

				</tr>
			<?php
			}
		} else { ?>

		<?php }	?>
	</table>
	<table  class="table center" style="margin-top: 20px;">
		<tr>
			<td> <a href="../crew/input_crew.php"><b>INPUT CREW</b>  ||</a>
			</td>
			<td> <a href="../destinasi/input_tujuan.php"><b>INPUT TUJUAN</b> ||</a>
			</td>
			<td> <a href="../armada/input_armada.php"><b>INPUT ARMADA</b> ||</a>
			</td>			
			<td> <a href="../order/input.php"><b>INPUT PENJADWALAN</b></a>
			</td>
		</tr>
	</table>
</body>

</html>