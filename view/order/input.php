<?php
include '../../koneksi/koneksi.php';          //agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include
?>
<html>

<body>
    <form method="post" action="../../controller/penjadwalan.php">
        <table>
            <tr>
                <td>Nama</td>
                <td><input type="text" name="nama"></td>
            </tr>
            <tr>
                <td>Alamat </td>
                <td><input type="text" name="alamat"></td>
            </tr>

            <tr>
                <td>Armada</td>
                <td colspan="2">
                    <select id="cars" name="armada">
                        <?php
                        $query1 = "SELECT * FROM armada ORDER BY id DESC";

                        $result1 = mysqli_query($koneksi, $query1);

                        while ($data = mysqli_fetch_assoc($result1)) {
                        ?>

                            <option value="<?php echo $data['id']; ?>"><?php echo 'Armada = ' . $data['jenis_armada']; ?> || <?php echo 'Jumlah Seat = ' . $data['jumlah_seat']; ?></option>

                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Crew</td>
                <td colspan="2">
                    <select id="cars" name="crew">
                        <?php
                        $query = "SELECT * FROM crew ORDER BY id DESC";

                        $result = mysqli_query($koneksi, $query);

                        while ($data = mysqli_fetch_assoc($result)) {
                        ?>

                            <option value="<?php echo $data['id']; ?>"><?php echo $data['nama']; ?></option>

                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Destinasi</td>
                <td colspan="2">
                    <select id="cars" name="tujuan">
                        <?php
                        $query2 = "SELECT * FROM tujuan ORDER BY id DESC";

                        $result2 = mysqli_query($koneksi, $query2);

                        while ($data2 = mysqli_fetch_assoc($result2)) {
                        ?>

                            <option value="<?php echo $data2['id']; ?>"><?php echo $data2['nama'].'('.$data2['lokasi'].')'; ?></option>

                        <?php } ?>
                    </select>
                </td>
            </tr>


            <tr>
                <td>Jumlah Armada</td>
                <td><input type="number" name="jml_armada"></td>
            </tr>
            <tr>
                <td>Lokasi Penemput</td>
                <td><input type="text" name="lok_jemput"></td>
            </tr>
            <tr>
                <td>Jam</td>
                <td><input type="time" name="jam"></td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td><input type="date" name="tanggal"></td>
            </tr>
            <tr>
                <td>Biaya</td>
                <td><input type="number" name="biaya"></td>
            </tr>
            <tr>
                <td colspan="2"><button type="submit" value="next">Tambah</button></td>
            </tr>
        </table>
    </form>
</body>

</html>